package gofunk

import (
	"strconv"
)

func ReverseMapIntInt(m map[int]int) map[int]int {
	n := make(map[int]int)
	for k, v := range m {
		n[v] = k
	}
	return n
}

func ReverseMapStrStr(m map[string]string) map[string]string {
	n := make(map[string]string)
	for k, v := range m {
		n[v] = k
	}
	return n
}

func ReverseMapIntStr(m map[int]string) map[int]string {
	n := make(map[int]string)
	for k, v := range m {
		vi, _ := strconv.Atoi(v)
		n[vi] = strconv.Itoa(k)
	}
	return n
}

func ReverseMapStrInt(m map[string]int) map[string]int {
	n := make(map[string]int)
	for k, v := range m {
		vs := strconv.Itoa(v)
		ki, _ := strconv.Atoi(k)
		n[vs] = ki
	}
	return n
}